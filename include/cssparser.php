<?php

class CSSParser {

	private $raw_css;
	public $parsed_css;
	public $one_line_at_rules;


	public function __construct( $raw_css ) {

		$raw_css       = str_replace( array( "\r", "\n", "\t", "  ", "    " ), ' ', $raw_css );
		$this->raw_css = preg_replace( '!/\*.*?\*/!', '', $raw_css );

	}

	/**
	 * Parse CSS
	 */
	public function split_css() {

		$raw_css = $this->raw_css;

		$one_line_at_rules = array();

		preg_match_all( '/@import.*?;/i', $raw_css, $one_line_at_rules[0] );
		preg_match_all( '/@namespace.*?;/i', $raw_css, $one_line_at_rules[1] );
		preg_match_all( '/@charset.*?;/i', $raw_css, $one_line_at_rules[2] );

		$this->one_line_at_rules = $one_line_at_rules;

		// remove not supported at-rules
		$raw_css = preg_replace( '/@import.*?;/i', '', $raw_css );
		$raw_css = preg_replace( '/@charset.*?;/i', '', $raw_css );
		$raw_css = preg_replace( '/@namespace.*?;/i', '', $raw_css );


		//split rule-sets
		preg_match_all( '/[^{]*({(?>[^{}]++|(?1))*})/', $raw_css, $rules );

		$rule_sets = array();

		//for each rule-set
		foreach ( $rules[0] as $key => $rule ) {
			$rule = trim( $rule, " \t\n\r\0\x0B" );

			//if at rule is media
			if ( preg_match( '/^@.*?media/i', $rule ) || preg_match( '/^@.*?supports/i', $rule ) || preg_match( '/^@.*?keyframes/i', $rule ) || preg_match( '/^@.*?document/i', $rule ) || preg_match( '/^@.*?font-feature-values/i', $rule ) ) {

				preg_match( '/(@.*?)\{(.*)\}/i', $rule, $split_media );
				$media = trim( $split_media[1] );

				preg_match_all( '/[^{]*({(?>[^{}]++|(?1))*})/', $split_media[2], $media_rules );
				foreach ( $media_rules[0] as $media_rule ) {

					$split_rule = explode( '{', trim( $media_rule, '}' ) );

					$selector           = trim( $split_rule[0] );
					$split_declarations = explode( ';', trim( $split_rule[1], "; \t\n\r\0\x0B" ) );
					foreach ( $split_declarations as $declaration ) {
						$declaration = explode( ':', trim( $declaration ) );

						$rule_sets[ $media ][ $selector ][ trim( $declaration[0] ) ] = trim( $declaration[1] );
					}
				}
			} else {

				$split_rule         = explode( '{', trim( $rule, '}' ) );
				$selector           = trim( $split_rule[0] );
				$split_declarations = explode( ';', trim( $split_rule[1], "; \t\n\r\0\x0B" ) );

				foreach ( $split_declarations as $declaration ) {
					$declaration                                       = explode( ':', trim( $declaration ) );
					$rule_sets[ $selector ][ trim( $declaration[0] ) ] = trim( $declaration[1] );
				}

			}
		}

		$this->parsed_css = $rule_sets;
	}

	/**
	 * get other defined properties from parsed css for this element
	 *
	 * @param string $selector
	 *
	 * @return array
	 */
	public function get_defined_properties( $selector ) {

		$split_selector = explode( ' ', $selector );

		$split_selector_count = count( $split_selector );
		$index                = array();

		for ( $i = 0; $i < $split_selector_count; $i ++ ) {

			array_shift( $split_selector );
			$search_selector = implode( ' ', $split_selector );

			if ( array_key_exists( $search_selector, $this->parsed_css ) ) {
				$index[ $search_selector ] = $this->parsed_css[ $search_selector ];
			}

		}

		$keys = array_map( 'strlen', array_keys( $index ) );
		array_multisort( $keys, SORT_ASC, $index );

		$defined_properties = array();
		foreach ( $index as $selector ) {
			foreach ( $selector as $property => $value ) {
				$defined_properties[ $property ] = $value;
			}
		}

		return $defined_properties;
	}

}