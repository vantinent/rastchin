<?php

class Convert {

	public $parsed_css;
	public $one_line_at_rules;
	public $rtl;
	public $rtl_parsed_css;
	public $rtl_property_count = 0;
	public $property_count = 0;

	public function __construct( $raw_css ) {

		$parser = new CSSParser( $raw_css );
		$parser->split_css();
		$this->parsed_css        = $parser->parsed_css;
		$this->one_line_at_rules = $parser->one_line_at_rules;
		$this->rtl               = new RTL();
	}

	public function get_rtl( $complete = false, $reset = false, $minify = false ) {

		$this->rtl->reset = $reset;
		$this->run( $complete );

		return $this->get_rtl_raw_css( $minify, $complete );

	}


	public function run( $complete = false ) {

		$rtl_parsed = array();
		foreach ( $this->parsed_css as $selector => $declaration ) {

			if ( preg_match( '/^@.*?media.*?/i', $selector ) || preg_match( '/^@.*?supports.*?/i', $selector ) || preg_match( '/^@.*?keyframes.*?/i', $selector ) || preg_match( '/^@.*?document.*?/i', $selector ) || preg_match( '/^@.*?font-feature-values.*?/i', $selector ) ) {
				$media_name = $selector;
				$media      = $declaration;

				foreach ( $media as $media_selector => $media_declaration ) {
					foreach ( $media_declaration as $property => $value ) {
						// if keyframes ignore convert
						if ( preg_match( '/^@.*?keyframes.*?/i', $media_name ) || preg_match( '/^@.*?font-feature-values.*?/i', $media_name ) ) {
							if ( $complete ) {
								$rtl_parsed[ $media_name ][ $media_selector ][ $property ] = $value;
							}
						} else {
							$rtl = $this->convert( $value, $property, $media_selector, $media_name );

							if ( $rtl ) {
								foreach ( $rtl as $new_property => $new_value ) {
									$rtl_parsed[ $media_name ][ $media_selector ][ $new_property ] = $new_value;

								}
								$this->rtl_property_count ++;
							} elseif ( $complete ) {
								$rtl_parsed[ $media_name ][ $media_selector ][ $property ] = $value;
							}
						}
						$this->property_count ++;
					}
				}
			} else {

				foreach ( $declaration as $property => $value ) {
					$rtl = $this->convert( $value, $property, $selector );

					if ( $rtl ) {
						foreach ( $rtl as $new_property => $new_value ) {
							$rtl_parsed[ $selector ][ $new_property ] = $new_value;

						}
						$this->rtl_property_count ++;

					} elseif ( $complete ) {
						$rtl_parsed[ $selector ][ $property ] = $value;
					}

					$this->property_count ++;
				}
			}
		}

		$this->rtl_parsed_css = $rtl_parsed;
	}

	public function get_rtl_raw_css( $minify = false, $complete = false ) {

		$newline = "\n";
		$tab     = "    ";
		$space   = " ";

		if ( $minify ) {
			$newline = "";
			$tab     = "";
			$space   = "";
		}

		$rtl_raw_css = '';

		//one line at rules
		if ( $complete ) {
			foreach ( $this->one_line_at_rules as $value ) {
				foreach ( $value[0] as $at_rule ) {
					$rtl_raw_css .= $at_rule . $newline;
				}
			}
		}

		foreach ( $this->rtl_parsed_css as $selector => $declaration ) {
			$rtl_raw_css .= $selector . '{' . $newline;

			if ( preg_match( '/^@.*?media.*?/i', $selector ) || preg_match( '/^@.*?supports.*?/i', $selector ) || preg_match( '/^@.*?keyframes.*?/i', $selector ) || preg_match( '/^@.*?document.*?/i', $selector ) || preg_match( '/^@.*?font-feature-values.*?/i', $selector ) ) {

				$media = $declaration;

				foreach ( $media as $media_selector => $media_declaration ) {
					$rtl_raw_css .= $tab . $media_selector . '{' . $newline;
					foreach ( $media_declaration as $property => $value ) {
						$rtl_raw_css .= $tab . $tab . $property . ':' . $space . $value . ';' . $newline;
					}
					$rtl_raw_css .= $tab . '}' . $newline . $newline;
				}
			} else {

				foreach ( $declaration as $property => $value ) {
					$rtl_raw_css .= $tab . $property . ':' . $space . $value . ';' . $newline;
				}
			}

			$rtl_raw_css .= '}' . $newline . $newline;
		}

		return $rtl_raw_css;
	}

	public function convert( $value, $property, $selector, $media = false ) {

		$selector_properties = ( $media ) ? $this->parsed_css[ $media ][ $selector ] : $this->parsed_css[ $selector ];


		$property = trim( $property );
		$value    = trim( $value );


		if ( $property === "text-align" ) {
			return $this->rtl->text_align( $value );

		} else if ( $property === "float" ) {
			return $this->rtl->float( $value );

		} else if ( $property === "direction" ) {
			return $this->rtl->direction( $value );

		} else if ( $property === "background" || $property === "background-position" || $property === "background-position-x" ) {
			return $this->rtl->background( $property, $value );

		} else if ( $property === "margin-left" ) {
			return ( array_key_exists( 'margin-right', $selector_properties ) )
				? $this->rtl->margin_left( $value, false )
				: $this->rtl->margin_left( $value );

		} else if ( $property === "margin-right" ) {
			return ( array_key_exists( 'margin-left', $selector_properties ) )
				? $this->rtl->margin_right( $value, false )
				: $this->rtl->margin_right( $value );

		} else if ( $property === "margin" ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( $property === "padding-left" ) {
			return ( array_key_exists( 'padding-right', $selector_properties ) )
				? $this->rtl->padding_left( $value, false )
				: $this->rtl->padding_left( $value );

		} else if ( $property === "padding-right" ) {
			return ( array_key_exists( 'padding-left', $selector_properties ) )
				? $this->rtl->padding_right( $value, false )
				: $this->rtl->padding_right( $value );

		} else if ( $property === "padding" ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( $property === "left" ) {
			return ( array_key_exists( 'right', $selector_properties ) )
				? $this->rtl->left( $value, false )
				: $this->rtl->left( $value );

		} else if ( $property === "right" ) {
			return ( array_key_exists( 'left', $selector_properties ) )
				? $this->rtl->right( $value, false )
				: $this->rtl->right( $value );

		} else if ( preg_match( "/clear/", $property ) ) {
			return $this->rtl->clear( $value );

		} else if ( $property === "border-left" ) {
			return ( array_key_exists( 'border-right', $selector_properties ) )
				? $this->rtl->border_left( $value, false )
				: $this->rtl->border_left( $value );

		} else if ( $property === "border-right" ) {
			return ( array_key_exists( 'border-left', $selector_properties ) )
				? $this->rtl->border_right( $value, false )
				: $this->rtl->border_right( $value );

		} else if ( $property === "border-bottom-right-radius" ) {
			return ( array_key_exists( 'border-bottom-left-radius', $selector_properties ) )
				? $this->rtl->border_bottom_right_radius( $value, false )
				: $this->rtl->border_bottom_right_radius( $value );

		} else if ( $property === "border-bottom-left-radius" ) {
			return ( array_key_exists( 'border-bottom-right-radius', $selector_properties ) )
				? $this->rtl->border_bottom_left_radius( $value, false )
				: $this->rtl->border_bottom_left_radius( $value );

		} else if ( $property === "border-top-right-radius" ) {
			return ( array_key_exists( 'border-top-left-radius', $selector_properties ) )
				? $this->rtl->border_top_right_radius( $value, false )
				: $this->rtl->border_top_right_radius( $value );

		} else if ( $property === "border-top-left-radius" ) {
			return ( array_key_exists( 'border-top-right-radius', $selector_properties ) )
				? $this->rtl->border_top_left_radius( $value, false )
				: $this->rtl->border_top_left_radius( $value );

		} else if ( $property === "border-left-color" ) {
			return ( array_key_exists( 'border-right-color', $selector_properties ) )
				? $this->rtl->border_left_color( $value, false )
				: $this->rtl->border_left_color( $value );

		} else if ( $property === "border-right-color" ) {
			return ( array_key_exists( 'border-left-color', $selector_properties ) )
				? $this->rtl->border_right_color( $value, false )
				: $this->rtl->border_right_color( $value );

		} else if ( $property === "border-left-style" ) {
			return ( array_key_exists( 'border-right-style', $selector_properties ) )
				? $this->rtl->border_left_style( $value, false )
				: $this->rtl->border_left_style( $value );

		} else if ( $property === "border-right-style" ) {
			return ( array_key_exists( 'border-left-style', $selector_properties ) )
				? $this->rtl->border_right_style( $value, false )
				: $this->rtl->border_right_style( $value );

		} else if ( $property === "border-left-width" ) {
			return ( array_key_exists( 'border-right-width', $selector_properties ) )
				? $this->rtl->border_left_width( $value, false )
				: $this->rtl->border_left_width( $value );

		} else if ( $property === "border-right-width" ) {
			return ( array_key_exists( 'border-left-width', $selector_properties ) )
				? $this->rtl->border_right_width( $value, false )
				: $this->rtl->border_right_width( $value );

		} else if ( preg_match( "/(.*?)border-radius/", $property ) ) {
			return $this->rtl->border_radius( $value );

		} else if ( preg_match( "/(.*?)border-color/", $property ) ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( preg_match( "/(.*?)border-image-slice/", $property ) ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( preg_match( "/(.*?)border-image-outse/", $property ) ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( preg_match( "/(.*?)border-image-width/", $property ) ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( preg_match( "/(.*?)border-style/", $property ) ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( preg_match( "/(.*?)border-width/", $property ) ) {
			return $this->rtl->four_section_property( $property, $value );

		} else if ( preg_match( "/(.*?)break-after/", $property ) ) {
			return $this->rtl->break_after( $value );

		} else if ( preg_match( "/(.*?)break-before/", $property ) ) {
			return $this->rtl->break_before( $value );

		} else if ( preg_match( "/(.*?)caption-side/", $property ) ) {
			return $this->rtl->caption_side( $value );

		}


		return null;
	}
}