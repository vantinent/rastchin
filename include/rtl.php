<?php

class RTL {

	public $reset = false;

	public function text_align( $value ) {

		if ( preg_match( '/(.*?)left(.*?)/', $value ) ) {
			return array( 'text-align' => str_replace( 'left', 'right', $value ) );
		} else if ( preg_match( '/(.*?)right(.*?)/', $value ) ) {
			return array( 'text-align' => str_replace( 'right', 'left', $value ) );
		}
	}

	public function float( $value ) {

		if ( preg_match( '/(.*?)left(.*?)/', $value ) ) {
			return array( 'float' => str_replace( 'left', 'right', $value ) );
		} else if ( preg_match( '/(.*?)right(.*?)/', $value ) ) {
			return array( 'float' => str_replace( 'right', 'left', $value ) );
		}
	}

	public function direction( $value ) {

		if ( preg_match( '/(.*?)ltr(.*?)/', $value ) ) {
			return array( 'direction' => str_replace( 'ltr', 'rtl', $value ) );
		} else if ( preg_match( '/(.*?)rtl(.*?)/', $value ) ) {
			return array( 'direction' => str_replace( 'rtl', 'ltr', $value ) );
		}
	}

	public function clear( $value ) {

		if ( preg_match( '/(.*?)left(.*?)/', $value ) ) {
			return array( 'clear' => str_replace( 'left', 'right', $value ) );
		} else if ( preg_match( '/(.*?)right(.*?)/', $value ) ) {
			return array( 'clear' => str_replace( 'right', 'left', $value ) );
		}
	}

	/**
	 * background, background-position, background-position-x
	 *
	 * @param $property
	 * @param $value
	 *
	 * @return array|null
	 */
	public function background( $property, $value ) {

		if ( ! preg_match( '/^(.*?)(left|right|%|px|\d)+(.*?)$/', $value ) ) {
			return null;
		}
		$background     = explode( ' ', $value );
		$new_background = array();
		$count          = 0;
		$fist_percent   = true;
		$fist_px        = true;
		$converted      = false;
		$first_zero     = true;
		foreach ( $background as $b_value ) {

			if ( preg_match( '/^(.*?)(url|"|\(\))+(.*?)$/', $b_value ) ) {

			} elseif ( preg_match( '/^(.*?)left(.*?)$/', $b_value ) ) {
				$b_value   = 'right';
				$converted = true;
			} elseif ( preg_match( '/^(.*?)right(.*?)$/', $b_value ) ) {
				$b_value   = 'left';
				$converted = true;
			} elseif ( $b_value === '0' ) {
				if ( $first_zero ) {
					$b_value   = 'right ';
					$converted = true;
				}
				$first_zero = false;
			} elseif ( preg_match( '/^(.*?)px(.*?)$/', $b_value ) ) {
				if ( $count > 0 && in_array( $background[ $count - 1 ], array( 'left', 'right', 'center', '/', '0', 'top', 'bottom' ) ) ) {

				} else if ( $fist_px ) {
					if ( isset( $background[ $count + 1 ] ) && preg_match( '/^(.*?)(top|right|center|%|px|\d)+(.*?)$/', $background[ $count + 1 ] ) ) {
						$b_value = 'right ' . $b_value;
					} else {
						$b_value = 'right ' . $b_value . ' center';
					}
					$converted = true;
				}
				$fist_px = false;
			} elseif ( preg_match( '/^(.*?)%(.*?)$/', $b_value ) ) {
				if ( $count > 0 && in_array( $background[ $count - 1 ], array( 'left', 'right', '0' ) ) ) {

				} else if ( $fist_px && $fist_percent ) {
					$b_value   = ( 100 - (int) str_replace( '%', '', $b_value ) ) . '%';
					$converted = true;
				}

				$fist_percent = false;
				$fist_px      = false;
			}

			$new_background[] = $b_value;
			$count ++;
		}

		if ( $converted ) {
			return array( $property => implode( ' ', $new_background ) );
		}

		return null;
	}

	/**
	 * property: top right bottom left;
	 *
	 * @param $property
	 * @param $value
	 *
	 * @return array
	 */
	public function four_section_property( $property, $value ) {

		$digit = explode( ' ', $value );
		if ( count( $digit ) >= 4 && $digit[3] != "!important" ) {

			$digit3 = explode( '!', $digit[3] );
			if ( count( $digit3 ) > 1 ) {
				$digit[3] = $digit3[0];
				$digit[4] = '!' . $digit3[1];
			}
			$important = ( isset( $digit[4] ) ) ? $digit[4] : "";
			$value     = $digit[0] . " " . $digit[3] . " " . $digit[2] . " " . $digit[1] . ' ' . $important;

			return array( $property => $value );
		}
	}

	public function margin_left( $value, $reset = true ) {

		if ( $this->reset && $reset && ! in_array( $value, array( '0', '0px', '0 px' ) ) ) {
			return array( "margin-right" => $value, 'margin-left' => 'auto' );
		}

		return array( "margin-right" => $value );
	}

	public function margin_right( $value, $reset = true ) {

		if ( $this->reset && $reset && ! in_array( $value, array( '0', '0px', '0 px' ) ) ) {
			return array( "margin-left" => $value, 'margin-right' => 'auto' );
		}

		return array( "margin-left" => $value );
	}

	public function padding_left( $value, $reset = true ) {

		if ( $this->reset && $reset && ! in_array( $value, array( '0', '0px', '0 px' ) ) ) {
			return array( "padding-right" => $value, 'padding-left' => '0' );
		}

		return array( "padding-right" => $value );
	}

	public function padding_right( $value, $reset = true ) {

		if ( $this->reset && $reset && ! in_array( $value, array( '0', '0px', '0 px' ) ) ) {
			return array( "padding-left" => $value, 'padding-right' => '0' );
		}

		return array( "padding-left" => $value );
	}

	public function right( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "left" => $value, 'right' => 'auto' );
		}

		return array( "left" => $value );
	}

	public function left( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "right" => $value, 'left' => 'auto' );
		}

		return array( "right" => $value );
	}

	public function border_left( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-right" => $value, 'border-left' => 'none' );
		}

		return array( "border-right" => $value );
	}

	public function border_right( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-left" => $value, 'border-right' => 'none' );
		}

		return array( "border-left" => $value );
	}

	public function border_top_left_radius( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-top-right-radius" => $value, 'border-top-left-radius' => 'none' );
		}

		return array( "border-top-right-radius" => $value );
	}

	public function border_top_right_radius( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-top-left-radius" => $value, 'border-top-right-radius' => 'none' );
		}

		return array( "border-top-left-radius" => $value );
	}

	public function border_bottom_left_radius( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-bottom-right-radius" => $value, 'border-bottom-left-radius' => 'none' );
		}

		return array( "border-bottom-right-radius" => $value );
	}

	public function border_bottom_right_radius( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-bottom-left-radius" => $value, 'border-bottom-right-radius' => 'none' );
		}

		return array( "border-bottom-left-radius" => $value );
	}

	public function border_right_color( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-left-color" => $value, 'border-right-color' => 'unset' );
		}

		return array( "border-left-color" => $value );
	}

	public function border_left_color( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-right-color" => $value, 'border-left-color' => 'unset' );
		}

		return array( "border-right-color" => $value );
	}

	public function border_left_style( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-right-style" => $value, 'border-left-style' => 'unset' );
		}

		return array( "border-right-style" => $value );
	}

	public function border_right_style( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-left-style" => $value, 'border-right-style' => 'unset' );
		}

		return array( "border-left-style" => $value );
	}

	public function border_left_width( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-right-width" => $value, 'border-left-width' => 'unset' );
		}

		return array( "border-right-width" => $value );
	}

	public function border_right_width( $value, $reset = true ) {

		if ( $this->reset && $reset ) {
			return array( "border-left-width" => $value, 'border-right-width' => 'unset' );
		}

		return array( "border-left-width" => $value );
	}

	public function border_radius( $value ) {

		$digit = explode( ' ', $value );
		if ( count( $digit ) >= 4 ) {
			$important = ( isset( $digit[4] ) ) ? " $digit[4]" : "";
			$value     = $digit[1] . " " . $digit[0] . " " . $digit[3] . " " . $digit[2] . $important;

			return array( 'border-radius' => $value );
		}
	}

	public function break_after( $value ) {

		if ( preg_match( '/(.*?)left(.*?)/', $value ) ) {
			return array( 'break-after' => str_replace( 'left', 'right', $value ) );
		} else if ( preg_match( '/(.*?)right(.*?)/', $value ) ) {
			return array( 'break-after' => str_replace( 'right', 'left', $value ) );
		}
	}

	public function break_before( $value ) {

		if ( preg_match( '/(.*?)left(.*?)/', $value ) ) {
			return array( 'break-before' => str_replace( 'left', 'right', $value ) );
		} else if ( preg_match( '/(.*?)right(.*?)/', $value ) ) {
			return array( 'break-before' => str_replace( 'right', 'left', $value ) );
		}
	}

	public function caption_side( $value ) {

		if ( preg_match( '/(.*?)left(.*?)/', $value ) ) {
			return array( 'caption-side' => str_replace( 'left', 'right', $value ) );
		} else if ( preg_match( '/(.*?)right(.*?)/', $value ) ) {
			return array( 'caption-side' => str_replace( 'right', 'left', $value ) );
		}
	}


}