<?php

define( 'DOMAIN', 'https://rtl.daskhat.ir' );
define( 'AJAX_URL', 'https://rtl.daskhat.ir/convert.php' );
define( 'PRODUCTION', true );

// Turn off error reporting
if ( PRODUCTION ) {
	error_reporting( 0 );
	@ini_set( 'display_errors', 0 );
}


